package com.rainmakeross.lucene

import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document.{Document, Field, FloatField, IntField, StringField}
import org.apache.lucene.index.{DirectoryReader, IndexWriter, IndexWriterConfig, Term}
import org.apache.lucene.search.{ScoreDoc, BooleanClause, BooleanQuery, IndexSearcher, TermQuery}
import org.apache.lucene.store.{FSDirectory}

import scala.collection.mutable

object BasicLucene extends App{
  val lucenePathString = "tmp/lucene"
  val lucenePath = new java.io.File(lucenePathString).toPath

  val analyzer = new StandardAnalyzer()
  val directory = FSDirectory.open(lucenePath)
  val iwc = new IndexWriterConfig(analyzer)
  val writer = new IndexWriter(directory, iwc)
  //val searcher = new IndexSearcher(DirectoryReader.open(directory))
  val book1 = new Book("Midsummer night's dream","William Shakespeare", "Plays", 300, 1000)

  // First time run, use createIndex to create index
  //createIndex()

  // Add a sample book
  //addBookToIndex(book1)

  // demonstrate search results
  //println(searchForField("author", "William Shakespeare").head)

  def createIndex(): Unit ={
    iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND)
    writer.commit()
  }

  def addBookToIndex(bookV0: Book): Unit ={
    val document = new Document()
    document.add(new StringField("title", bookV0.bookName, Field.Store.YES))
    document.add(new StringField("author", bookV0.author, Field.Store.YES))
    document.add(new StringField("category", bookV0.category, Field.Store.YES))
    document.add(new IntField("numpage", bookV0.numPages, Field.Store.YES))
    document.add(new FloatField("price", bookV0.price, Field.Store.YES))
    writer.addDocument(document)
    writer.commit()
  }

  def searchForField(field:String, value:String): List[Book] ={
    val searcher = new IndexSearcher(DirectoryReader.open(directory))
    val query = new BooleanQuery
    query.add(new TermQuery(new Term(field, value)), BooleanClause.Occur.MUST)
    val numResults = 100
    val hits =   searcher.search(query,numResults).scoreDocs
    val bookList:mutable.MutableList[Book] = mutable.MutableList()
    for(x <- hits){
      bookList += getBook(searcher.doc(x.doc))
    }
    bookList.toList
  }

  def getBook(doc:Document): Book ={
    new Book( doc.get("title"), doc.get("author"), doc.get("category"), doc.get("numpage").toInt, doc.get("price").toFloat)
  }


}
